class CreateEntries < ActiveRecord::Migration
  def self.up
    create_table :entries do |t|
      t.string    :title
      t.text      :body
      t.datetime :due_at


      t.timestamps
    end
  end

  def self.down
    drop_table :entries
  end
end
